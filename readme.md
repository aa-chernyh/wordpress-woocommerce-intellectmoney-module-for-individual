#Модуль оплаты платежной системы IntellectMoney для CMS Wordpress с модулем wooCommerce для физических лиц

> **Внимание!** <br>
Данная версия актуальна на *25 марта 2020 года* и не обновляется. <br>
Актуальную версию можно найти по ссылке https://wiki.intellectmoney.ru/pages/viewpage.action?pageId=2098184#wooCommerce_files.

Инструкция по настройке доступна по ссылке https://wiki.intellectmoney.ru/pages/viewpage.action?pageId=2098184#2098184e834ad05142849aab4f20ce91ea7114c