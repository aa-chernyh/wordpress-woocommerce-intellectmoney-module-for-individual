<?php
/*
  Plugin Name: Платежный модуль IntellectMoney
  Plugin URI:
  Description: IntellectMoney — это универсальное решение по приему платежей в интернете. Мы работаем на рынке онлайн-платежей более 5 лет. За это время нас выбрали более 6 000 компаний и 17 кредитных организаций. Среди них как российские, так и иностранные компании.
  Version: 1.0.0
  Author: IntellectMoney
  Author URI: https://intellectmoney.ru
 */
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
if (!defined('ABSPATH'))
    exit;

add_action('plugins_loaded', 'woocommerce_intellectmoney', 0);

function woocommerce_intellectmoney() {
    if (!class_exists('WC_Payment_Gateway'))
        return;
    if (class_exists('WC_IntellectMoney'))
        return;

    class WC_IntellectMoney extends WC_Payment_Gateway {

        var $outsumcurrency = '';
        var $lang;
        private $im_eshopId;
        public function __construct() {
            $plugin_dir = plugin_dir_url(__FILE__);

            global $woocommerce;

            $this->id = 'intellectmoney';
            $this->icon = apply_filters('woocommerce_intellectmoney_icon', '' . $plugin_dir . 'logo.png');
            $this->has_fields = false;

            $this->init_form_fields();
            $this->init_settings();

            // Define user set variables
            $this->title = $this->get_option('im_name');
            $this->im_eshopId = $this->get_option('im_eshopId');
            $this->im_secretKey = $this->get_option('im_secretKey');
            $this->im_hold = $this->get_option('im_hold');
            $this->im_expireDate = $this->get_option('im_expireDate');
            $this->im_productnds = $this->get_option('im_productnds');
            $this->im_deliverynds = $this->get_option('im_deliverynds');
            $this->im_preference = $this->get_option('im_preference');
            $this->im_successUrl = $this->get_option('im_successUrl');
            $this->im_paymentStatus = array(
				substr($this->get_option('im_paymentStatus3'),3) => 3,
				substr($this->get_option('im_paymentStatus4'),3) => 4,
				substr($this->get_option('im_paymentStatus5'),3) => 5,
				substr($this->get_option('im_paymentStatus6'),3) => 6,
				substr($this->get_option('im_paymentStatus7'),3) => 7,
				substr($this->get_option('im_paymentStatus8'),3) => 8
			);
            $this->im_accountId = $this->get_option('im_accountId');
            $this->im_formId = $this->get_option('im_formId');
            $this->im_formType = $this->get_option('im_formType');

            // Actions
            add_action('woocommerce_receipt_' . $this->id, array($this, 'receipt_page'));

            // Save options
            add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));

            // Payment listener/API hook
            add_action('woocommerce_api_wc_' . $this->id, array($this, 'check_ipn_response'));

            if (!$this->is_valid_for_use()) {
                $this->enabled = false;
            }
        }

        function is_valid_for_use() {
            if (!in_array(get_option('woocommerce_currency'), array('RUB', 'TST'))) {
                return false;
            }
            return true;
        }

        public function admin_options() {
            ?>
            <h3><?php _e('Платежный модуль IntellectMoney', 'woocommerce'); ?></h3>
            <p><?php _e('Настройка модуля оплаты IntellectMoney', 'woocommerce'); ?></p>

            <?php if ($this->is_valid_for_use()) : ?>
                <table class="form-table">

                    <?php
                    $this->generate_settings_html();
                    ?>
                </table>
                <script>
                    var el = document.getElementById('woocommerce_intellectmoney_im_resultUrl').setAttribute('disabled', 'disabled');
                </script>
            <?php else : ?>
                <div class="inline error"><p><strong><?php _e('Способ оплаты отключен', 'woocommerce'); ?></strong>: <?php _e('Модуль оплаты не поддерживает валюты Вашего магазина.', 'woocommerce'); ?></p></div>
            <?php
            endif;
        }

        function init_form_fields() {
            $statuses = wc_get_order_statuses();
            
            $formTypes = array(
                'IMAccount' => 'В личный кабинет',
                'PeerToPeer' => 'На карту'
            );

            $this->form_fields = array(
                'enabled' => array(
                    'title' => __('Включить/Выключить', 'woocommerce'),
                    'type' => 'checkbox',
                    'label' => __('Включен', 'woocommerce'),
                    'default' => 'yes'
                ),
                'im_name' => array(
                    'title' => __('Название', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('Название способа оплаты, которое будет отображаться при выборе', 'woocommerce'),
                    'default' => 'Оплата через IntellectMoney',
                    'placeholder' => 'Оплата через IntellectMoney',
                    'css' => 'width: 500px;'
                ),
                'im_description' => array(
                    'title' => __('Description', 'woocommerce'),
                    'type' => 'textarea',
                    'description' => __('Описание способа оплаты, которое будет отображаться при выборе', 'woocommerce'),
                    'default' => 'Оплата с помощью пластиковых карт Visa/Mastercard, СбербанкОнлайн, Яндекс.Деньги, Webmoney, Деньги@Mail.ru, терминалы оплаты, банковский перевод, почта России и т.д.',
                    'placeholder' => 'Оплата с помощью пластиковых карт Visa/Mastercard, СбербанкОнлайн, Яндекс.Деньги, Webmoney, Деньги@Mail.ru, терминалы оплаты, банковский перевод, почта России и т.д.',
                    'css' => 'width: 500px;'
                ),
                
                'im_eshopId' => array(
                    'title' => __('eshopId (Номер магазина в системе IntellectMoney)', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('Обычно используется 450157', 'woocommerce'),
                    'placeholder' => '450157',
                ),
                
                'im_accountId' => array(
                    'title' => __('AccountId (Номер аккаунта физического лица IntellectMoney)', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('Указано в личном кабинете IntellectMoney либо в коде формы приема платежей, параметр AccountId', 'woocommerce'),
                    'placeholder' => 'Например, 1530540937',
                ),

                'im_formId' => array(
                    'title' => __('FormId (Номер формы приема платежей)', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('Указан в коде формы, параметр FormId', 'woocommerce'),
                    'placeholder' => 'Например, 1228',
                ),                
                
                'im_formType' => array(
                    'title' => __('Прием платежей', 'woocommerce'),
                    'type' => 'select',
                    'description' => __('Должно соответствовать способу приёма денежных средств в форме приема платежей, выбирается при создании формы:"IMAccount"(В личный кабинет), "PeerToPeer" (На карту)', 'woocommerce'),
                    'options' => $formTypes,
                    'css' => 'width: 280px;'
                ),

                'im_secretKey' => array(
                    'title' => __('Секретный ключ в системе IntellectMoney', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('Укажите секретный ключ, такой же, который вы указали в личном кабинете IntellectMoney', 'woocommerce'),
                    'placeholder' => 'mySecretKey',
                    'css' => 'width: 500px;'
                ),
                'im_paymentStatus3' => array(
                    'title' => __('Счёт создан', 'woocommerce'),
                    'type' => 'select',
                    'description' => __('Укажите в какой статус переводить заказ, когда счёт в системе IntellectMoney создан', 'woocommerce'),
                    'options' => $statuses,
                    'css' => 'width: 180px;'
                ),
                'im_paymentStatus4' => array(
                    'title' => __('Счёт отменён', 'woocommerce'),
                    'type' => 'select',
                    'description' => __('Укажите в какой статус переводить заказ, когда счёт в системе IntellectMoney отменен', 'woocommerce'),
                    'options' => $statuses,
                    'css' => 'width: 180px;'
                ),
                'im_paymentStatus5' => array(
                    'title' => __('Счёт оплачен', 'woocommerce'),
                    'type' => 'select',
                    'description' => __('Укажите в какой статус переводить заказ, когда счёт в системе IntellectMoney полностью оплачен', 'woocommerce'),
                    'options' => $statuses,
                    'css' => 'width: 180px;'
                ),
                'im_paymentStatus6' => array(
                    'title' => __('Счёт захолдирован', 'woocommerce'),
                    'type' => 'select',
                    'description' => __('Укажите в какой статус переводить заказ, когда счёт в системе IntellectMoney захолдирован', 'woocommerce'),
                    'options' => $statuses,
                    'css' => 'width: 180px;'
                ),
                'im_paymentStatus7' => array(
                    'title' => __('Счёт частично оплачен', 'woocommerce'),
                    'type' => 'select',
                    'description' => __('Укажите в какой статус переводить заказ, когда счёт в системе IntellectMoney частично оплачен', 'woocommerce'),
                    'options' => $statuses,
                    'css' => 'width: 180px;'
                ),
                'im_paymentStatus8' => array(
                    'title' => __('Возврат', 'woocommerce'),
                    'type' => 'select',
                    'description' => __('Укажите в какой статус переводить заказ, когда был произведен возврат денежных средств', 'woocommerce'),
                    'options' => $statuses,
                    'css' => 'width: 180px;'
                ),
                'im_preference' => array(
                    'title' => __('Доступные способы оплаты', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('По умолчанию доступны все способы оплаты. Оставьте данное поле пустым, если не знаете что указать. Если платежи принимаются на карту, необходимо указать "bankcard"', 'woocommerce'),
                    'default' => '',
                    'placeholder' => 'bankcard, sberbank, yandex, webmoney, terminals, alfaclick',
                    'css' => 'width: 500px;'
                ),
                'im_successUrl' => array(
                    'title' => __('Адрес при успешной оплате', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('Адрес, на который будет перенаправлен клиент после успешной оплаты. Можно оставить по-умолчанию', 'woocommerce'),
                    'default' => get_site_url() . '/?wc-api=wc_intellectmoney&return=success_url',
                    'placeholder' => get_site_url() . '/?wc-api=wc_intellectmoney&return=success_url',
                    'css' => 'width: 500px;'
                ),
            );
        }

        function payment_fields() {
            if ($this->description) {
                echo wpautop(wptexturize($this->description));
            }
        }

        public function generate_form($order_id) {
            global $woocommerce;

            $order = new WC_Order($order_id);

            $recipientAmount = number_format($order->order_total, 2, '.', '');

            $control_hash_str = implode('::', array(
                $this->im_eshopId, $order_id, 'Оплата заказа №'. $order_id,
                $recipientAmount, $this->im_test == 'yes' ? 'TST' : $this->currency, $this->im_secretKey
            ));
            $control_hash = md5($control_hash_str);
            $email = $order->billing_email;

            $fields = array(
                'eshopId' => $this->im_eshopId,
                'orderId' => $order_id,
                'serviceName' => __('Оплата заказа №', 'woocommerce') . $order_id,
                'recipientAmount' => $recipientAmount,
                'recipientCurrency' => 'RUB',
                'userName' => $order->billing_first_name . ' ' . $order->billing_last_name,
                'email' => $email,
                'preference' => $this->im_formType == 'PeerToPeer' ? 'bankcard' : $this->im_preference,
                'successUrl' => $this->im_successUrl . '&orderId=' . $order->id,
                'hash' => $control_hash,
                'UserFieldName_0' => 'Перевод в кошелек',
                'UserFieldName_9' => 'UserPaymentFormId',
                'UserField_0' => $this->im_accountId,
                'UserField_9' => $this->im_formId,
                'FormType' => $this->im_formType,
                
            );

            $postData = array();
            foreach ($fields as $key => $value) {
                $postData[] = '<input type="hidden" name="' . esc_attr($key) . '" value="' . esc_attr($value) . '" />';
            }

            return
                    '<form id="im_pay" action="https://merchant.intellectmoney.ru/ru/" method="POST">' . "\n" .
                    implode("\n", $postData) . "\n" .
                    '</form>' .
                    '<script>
                        if (window.jQuery) {
                            jQuery(document).ready(function() { jQuery("#im_pay").submit(); });
                        }
                        else{
                            google.load("jquery", "1.4.3");
                            google.setOnLoadCallback(function() {
                                jQuery("#im_pay").submit(); 
                            }); 
                        }
                    </script>';
        }

        function process_payment($order_id) {
            $order = new WC_Order($order_id);
            if (!version_compare(WOOCOMMERCE_VERSION, '2.1.0', '<'))
                return array(
                    'result' => 'success',
                    'redirect' => $order->get_checkout_payment_url(true)
                );
            return array(
                'result' => 'success',
                'redirect' => add_query_arg('order-pay', $order->id, add_query_arg('key', $order->order_key, get_permalink(woocommerce_get_page_id('pay'))))
            );
        }

        function receipt_page($order) {
            echo '<p>' . __('Сейчас Вы будете перемещены на страницу оплаты', 'woocommerce') . '</p>';
            echo $this->generate_form($order);
        }

        function ob_exit($status = null) {
            if ($status) {
                ob_end_flush();
                exit($status);
            } else {
                ob_end_clean();
                header("HTTP/1.0 200 OK");
                echo "OK";
                exit();
            }
        }

        function check_im() {
            return in_array($_SERVER['REMOTE_ADDR'], array("194.147.107.254", "91.212.151.242", "127.0.0.1"));
        }

        function checkRequest($posted) {
            ob_start();
            list($eshopId, $orderId, $serviceName, $eshopAccount, $recipientAmount, $recipientCurrency, $paymentStatus, $userName, $userEmail, $paymentData, $secretKey, $hash) = array($_REQUEST['eshopId'], $_REQUEST['orderId'], $_REQUEST['serviceName'], $_REQUEST['eshopAccount'], $_REQUEST['recipientAmount'], $_REQUEST['recipientCurrency'], $_REQUEST['paymentStatus'], $_REQUEST['userName'], $_REQUEST['userEmail'], $_REQUEST['paymentData'], $_REQUEST['secretKey'], $_REQUEST['hash']);
            $order = new WC_Order($orderId);

            // Проверка по секретному ключу
            if ($secretKey) {
                //Проверка источника данных (по секретному ключу)
                if ($secretKey != $this->im_secretKey) {
                    $err = "ERROR: SECRET_KEY MISMATCH!\n";
                    $err .= $this->check_im() ? ("secretKey: $secretKey; order_secretKey: " . $this->im_secretKey . ";\n\n") : "\n";
                    $this->ob_exit($err);
                }

                //Проверка номера сайта продавца
                if ($eshopId != $this->im_eshopId) {
                    $err = "ERROR: INCORRECT ESHOP_ID!\n";
                    $err .= "eshopId: $eshopId; order_eshopId: " . $this->im_eshopId . ";\n\n";
                    $this->ob_exit($err);
                }
            }
            // Проверка по контрольной подписи
            else {
                $control_hash_str = implode('::', array(
                    
                    $eshopId, $orderId, $serviceName,
                    $eshopAccount, $recipientAmount, $recipientCurrency, $paymentStatus,
                    $userName, $userEmail, $paymentData, $this->im_secretKey
                ));

                $control_hash = md5($control_hash_str);
                $control_hash_utf8_str = (iconv('windows-1251', 'utf-8', $control_hash_str));
                $control_hash_utf8 = md5($control_hash_utf8_str);

                if (($hash != $control_hash && $hash != $control_hash_utf8) || !$hash) {

                    $err = "ERROR: HASH MISMATCH\n";
                    $err .= "Control hash string: $control_hash_str;\n";
                    $err .= "Control hash win-1251: $control_hash;\nControl hash utf-8: $control_hash_utf8;\nhash: $hash;\n\n";
                    $this->ob_exit($err);
                }
            }

        if ($this->isChangeOrder($order, $paymentStatus)) {
            $order->update_status($this->get_option('im_paymentStatus'.$paymentStatus));
        }
        $this->ob_exit();
    }

        function check_ipn_response() {
            global $woocommerce;

            if (isset($_GET['return']) AND $_GET['return'] == 'result_url') {
                @ob_clean();
                $_POST = stripslashes_deep($_POST);
                $this->checkRequest($_POST);
            } else if (isset($_GET['return']) AND $_GET['return'] == 'success_url') {
                $order = new WC_Order($_GET['orderId']);
                WC()->cart->empty_cart();
                wp_redirect($this->get_return_url($order));
            }
        }
		
		function isChangeOrder($order, $paymentStatus){
			$statuses = array(
				"2" => 0,
				"3" => 1,
				"7" => 2,
				"6" => 3,
				"5" => 4,
				"4" => 5,
			);
			return $statuses[$this->im_paymentStatus[$order->get_status()]] <= $statuses[$paymentStatus];
		}

    }

    function add_intellectmoney_gateway($methods) {
        $methods[] = 'WC_IntellectMoney';
        return $methods;
    }
    

    add_filter('woocommerce_payment_gateways', 'add_intellectmoney_gateway');
}

?>